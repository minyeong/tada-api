<?php

function add_tada_post_type( $args, $post_type ) {
	if ( 'tada_project' === $post_type ) {
		$args['show_in_rest'] = true;
	}
	return $args;
}
add_filter( 'register_post_type_args', 'add_tada_post_type', 10, 2 );


function wp_api_encode_acf($response, $post, $request) {
    if (isset($post)) {
        $acf = get_fields($post->ID);
				$url = wp_get_attachment_url( get_post_thumbnail_id($post->ID) );
        $response->data['featured_image_url'] = $url;
        $response->data['acf'] = $acf;
        $response->data['tada_item_tag'] = wp_get_object_terms($post->ID, 'tada_item_tag');;
    }
    return $response;
}

if( function_exists('get_fields') ){
	add_filter('rest_prepare_tada_project', 'wp_api_encode_acf', 10, 3);
}

add_filter('acf/settings/row_index_offset', '__return_zero');
