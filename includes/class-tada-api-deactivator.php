<?php

/**
 * Fired during plugin deactivation
 *
 * @link       https://wpteam.dev
 * @since      1.0.0
 *
 * @package    Tada_Api
 * @subpackage Tada_Api/includes
 */

/**
 * Fired during plugin deactivation.
 *
 * This class defines all code necessary to run during the plugin's deactivation.
 *
 * @since      1.0.0
 * @package    Tada_Api
 * @subpackage Tada_Api/includes
 * @author     wpteam <mins9919@naver.com>
 */
class Tada_Api_Deactivator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function deactivate() {

	}

}
