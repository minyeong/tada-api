<?php

/**
* The file that defines the core plugin class
*
* A class definition that includes attributes and functions used across both the
* public-facing side of the site and the admin area.
*
* @link       https://wpteam.dev
* @since      1.0.0
*
* @package    Tada_Api
* @subpackage Tada_Api/includes
*/

/**
* The core plugin class.
*
* This is used to define internationalization, admin-specific hooks, and
* public-facing site hooks.
*
* Also maintains the unique identifier of this plugin as well as the current
* version of the plugin.
*
* @since      1.0.0
* @package    Tada_Api
* @subpackage Tada_Api/includes
* @author     wpteam <mins9919@naver.com>
*/
class Tada_Api {

	/**
	* The loader that's responsible for maintaining and registering all hooks that power
	* the plugin.
	*
	* @since    1.0.0
	* @access   protected
	* @var      Tada_Api_Loader    $loader    Maintains and registers all hooks for the plugin.
	*/
	protected $loader;

	/**
	* The unique identifier of this plugin.
	*
	* @since    1.0.0
	* @access   protected
	* @var      string    $plugin_name    The string used to uniquely identify this plugin.
	*/
	protected $plugin_name;

	/**
	* The current version of the plugin.
	*
	* @since    1.0.0
	* @access   protected
	* @var      string    $version    The current version of the plugin.
	*/
	protected $version;

	/**
	* Define the core functionality of the plugin.
	*
	* Set the plugin name and the plugin version that can be used throughout the plugin.
	* Load the dependencies, define the locale, and set the hooks for the admin area and
	* the public-facing side of the site.
	*
	* @since    1.0.0
	*/
	public function __construct() {
		if ( defined( 'TADA_API_VERSION' ) ) {
			$this->version = TADA_API_VERSION;
		} else {
			$this->version = '1.0.0';
		}
		$this->plugin_name = 'tada-api';
		$this->load_dependencies();
		$this->define_api_hooks();

	}

	/**
	* Load the required dependencies for this plugin.
	*
	* Include the following files that make up the plugin:
	*
	* - Tada_Api_Loader. Orchestrates the hooks of the plugin.
	* - Tada_Api_i18n. Defines internationalization functionality.
	* - Tada_Api_Admin. Defines all hooks for the admin area.
	* - Tada_Api_Public. Defines all hooks for the public side of the site.
	*
	* Create an instance of the loader which will be used to register the hooks
	* with WordPress.
	*
	* @since    1.0.0
	* @access   private
	*/
	private function load_dependencies() {

		/**
		* The class responsible for orchestrating the actions and filters of the
		* core plugin.
		*/
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/class-tada-api-loader.php';


		/**
		* The class responsible for defining all actions that occur in the public-facing
		* side of the site.
		*/
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'api-bearer-auth/db.php';
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'api-bearer-auth/migrations.php';
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'api-bearer-auth/api-bearer-auth.php';

		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'api/class-tada-api-public.php';
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'api/query/class-posts.php';

		$this->loader = new Tada_Api_Loader();

	}

	/**
	* Register all of the hooks related to the public-facing functionality
	* of the plugin.
	*
	* @since    1.0.0
	* @access   private
	*/
	private function define_api_hooks() {
		$api_public = new Tada_Api_Public();
		$this->loader->add_action( 'rest_api_init', $api_public, 'register_routes' );

	}

	/**
	* Run the loader to execute all of the hooks with WordPress.
	*
	* @since    1.0.0
	*/
	public function run() {
		$this->loader->run();
	}

	/**
	* The name of the plugin used to uniquely identify it within the context of
	* WordPress and to define internationalization functionality.
	*
	* @since     1.0.0
	* @return    string    The name of the plugin.
	*/
	public function get_plugin_name() {
		return $this->plugin_name;
	}

	/**
	* The reference to the class that orchestrates the hooks with the plugin.
	*
	* @since     1.0.0
	* @return    Tada_Api_Loader    Orchestrates the hooks of the plugin.
	*/
	public function get_loader() {
		return $this->loader;
	}

	/**
	* Retrieve the version number of the plugin.
	*
	* @since     1.0.0
	* @return    string    The version number of the plugin.
	*/
	public function get_version() {
		return $this->version;
	}

}
