<?php

/**
 * Fired during plugin activation
 *
 * @link       https://wpteam.dev
 * @since      1.0.0
 *
 * @package    Tada_Api
 * @subpackage Tada_Api/includes
 */

/**
 * Fired during plugin activation.
 *
 * This class defines all code necessary to run during the plugin's activation.
 *
 * @since      1.0.0
 * @package    Tada_Api
 * @subpackage Tada_Api/includes
 * @author     wpteam <mins9919@naver.com>
 */
class Tada_Api_Activator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function activate() {

	}

}
