<?php

class Tada_Api_Public extends WP_REST_Controller {
	protected $data;

	public function __construct(){
		$this->data = new Tada_Posts_Query();
	}

	public function register_routes() {
		$namespace = 'tada';
		$versiton = 'v1';
		$full_namespace = $namespace.'/'.$versiton;
		$base = 'users/posts';
		register_rest_route( $full_namespace, '/' . $base, array(
			array(
				'methods'         => WP_REST_Server::READABLE,
				'callback'        => array( $this, 'get_items' ),
				'permission_callback' => array( $this, 'get_items_permissions_check' ),
				'args'            => array( $this->get_collection_params() ),
			),
			array(
				'methods'         => WP_REST_Server::CREATABLE,
				'callback'        => array( $this, 'create_item' ),
				'permission_callback' => array( $this, 'create_item_permissions_check' ),
				'args'            => $this->get_endpoint_args_for_item_schema( true ),
			),
		) );
		register_rest_route( $full_namespace, '/' . $base . '/(?P<id>[\d]+)', array(
			array(
				'methods'         => WP_REST_Server::READABLE,
				'callback'        => array( $this, 'get_item' ),
				'permission_callback' => array( $this, 'get_item_permissions_check' ),
				'args'            => array(
					'context'          => array(
						'default'      => 'view',
						'required'     => true,
					),
					'params' => array(
						'required'     => false,
						'id' => array(
							'description'        => 'The id(s) for the post(s) in the search.',
							'type'               => 'integer',
							'default'            => 1,
							'sanitize_callback'  => 'absint',
						),
					),
					$this->get_collection_params()
				),
			),
			array(
				'methods'         => 'POST',
				'callback'        => array( $this, 'update_item' ),
				'args'            => $this->get_endpoint_args_for_item_schema( false ),
			),
			array(
				'methods'  => WP_REST_Server::DELETABLE,
				'callback' => array( $this, 'delete_item' ),
				'permission_callback' => array( $this, 'delete_item_permissions_check' ),
				'args'     => array(
					'force'    => array(
						'default'      => false,
					),
				),
			),
		) );
		register_rest_route( $full_namespace, '/' . $base . '/schema', array(
			'methods'         => WP_REST_Server::READABLE,
			'callback'        => array( $this, 'get_public_item_schema' ),
		) );
		register_rest_route( $full_namespace, '/posts/categories', array(
			'methods'         => WP_REST_Server::READABLE,
			'callback'        => array( $this, 'get_terms' ),
		) );
		register_rest_field( 'tada_project', 'meta', array(
			'get_callback' => array($this, 'resgister_meta')
		) );

	}

	public function get_items( $request ) {
		$args = array(
			'author__in' => get_current_user_id(),
			'posts_per_page' => -1,
			'post_type' => 'tada_project'
		);
		$data = $this->data->query_post( $args );
		if ( !is_wp_error($data) ) {
			return new WP_REST_Response( $data, 200 );
		}else{
			return new WP_Error( 'cant-read', __( 'message', 'tada-api' ) );
		}
	}

	public function get_item( $request ) {
		$params = $request->get_params();
	  $data = $this->data->query_post( array( 'p' => $params[ 'id' ]) );

		if ( !is_wp_error($data) ) {
			return new WP_REST_Response( $data, 200 );
		}else{
			return new WP_Error( 'cant-read', __( 'message', 'tada-api' ) );
		}
	}

	public function create_item( $request ) {

		$temp = $request->get_params();
		$files = $request->get_file_params();
		$params = $this->prepare_item_for_insert($temp);

		if (!empty($files)) {
			$data = $this->data->insert_post( $params, $files );

			if ( is_array( $data ) && !is_wp_error($data) ) {
				return new WP_REST_Response( $data, 201 );
			} else {
				return new WP_REST_Response( $post_id->get_error_messages(), $post_id->get_error_code() );
			}
		}

		return new WP_Error( 'cant-create', __( 'message', 'tada-api'), array( 'status' => 500 ) );

	}


	public function update_item( $request ) {
		$temp = $request->get_params();
		$files = $request->get_file_params();
		$params = $this->prepare_item_for_update($temp);
		// $data = $this->data->update_acf( 'images', $files, 1047 );
		$data = $this->data->update_post( $params, $files );
		if ( is_array( $data ) && !is_wp_error($data) ) {
			return new WP_REST_Response( $data, 201 );
		} else {
			return new WP_REST_Response( $post_id->get_error_messages(), $post_id->get_error_code() );
		}

		return new WP_Error( 'cant-update', __( 'message', 'tada-api'), array( 'status' => 500 ) );

	}

	public function delete_item( $request ) {
		$params = $request->get_params();
		$id = $params['id'];
		$deleted = $this->data->delete_post($id);

			if (  !is_wp_error($deleted) &&  $deleted !== false ) {
				return new WP_REST_Response( [true], 200 );
			} else {
				return new WP_Error( 'cant-delete', __( 'message', 'tada-api'), array( 'status' => 500 ) );
		}
		return new WP_Error( 'cant-delete', __( 'message', 'tada-api'), array( 'status' => 500 ) );
	}

	public function get_terms( $request ) {
		$terms = $this->data->get_terms();
			if (  !is_wp_error($terms)  ) {
				return new WP_REST_Response( $terms, 200 );
			} else {
				return new WP_Error( 'cant-delete', __( 'message', 'tada-api'), array( 'status' => 500 ) );
		}
		return new WP_Error( 'cant-delete', __( 'message', 'tada-api'), array( 'status' => 500 ) );
	}

	public function get_items_permissions_check( $request ) {
		return true;
	}

	public function get_item_permissions_check( $request ) {
		return $this->get_items_permissions_check( $request );
	}

	public function create_item_permissions_check( $request ) {
		// return current_user_can( 'edit_something' );
		return true;
	}

	public function update_item_permissions_check( $request ) {
		return $this->create_item_permissions_check( $request );
	}

	public function delete_item_permissions_check( $request ) {
		return true;
		// return $this->create_item_permissions_check( $request );
	}

	protected function prepare_item_for_insert( $request ) {

		if ( isset( $request[ 'title' ] ) ) {
			$title= wp_strip_all_tags( $request[ 'title' ]  );
		} else {
			$title = '(임시저장)';
		}

		if ( isset( $request[ 'content' ] ) ) {
			$content = $request[ 'content' ];
		} else {
			$content = '';
		}

		if ( isset( $request[ 'tags' ] ) ) {
			$tags = $request[ 'tags' ];
		} else {
			$tags = '';
		}

		if ( isset( $request[ 'cat' ] ) ) {
			$cat = intval($request[ 'cat' ]);
		} else {
			$cat = '';
		}

		$item = array(
			'title'                   => $title,
			'content'                 => $content,
			'tags'                    => $tags,
			'cat'                     => $cat,
		);

		return $item;
	}


	protected function prepare_item_for_update( $request ) {
		$item = array();

		$item['ID'] = intval(wp_strip_all_tags($request['id']));

		if ( isset( $request[ 'title' ] ) ) {
			$item['title'] = wp_strip_all_tags( $request[ 'title' ]  );
		}

		if ( isset( $request[ 'content' ] ) ) {
				$item['content'] = $request[ 'content' ];
		} else {
			$item['content'] = '';
		}

		if ( isset( $request[ 'tags' ] ) ) {
			$item['tags'] = $request[ 'tags' ];
		} else {
			$item['tags'] = array();
		}

		if ( isset( $request[ 'cat' ] ) ) {
			$item['cat'] = intval($request[ 'cat' ]);
		}

		return $item;
	}

	public function prepare_item_for_response( $item, $request ) {

		$schema = $this->get_item_schema();
		$data   = array();
		$data = $item;
		$team = new Sports_Bench_Team( (int)$item[ 'team_id' ] );

		$data[ 'team_link' ] = $team->get_permalink();
		$data[ 'team_link' ] = str_replace( '&#038;', '&', $data[ 'team_link' ] );

		return $data;
	}

	public function get_collection_params() {
		return array(
			'team_id' => array(
				'description'        => 'The id(s) for the team(s) in the search.',
				'type'               => 'integer',
				'default'            => 1,
				'sanitize_callback'  => 'absint',
			),
			'team_name' => array(
				'description'        => 'The name(s) the team(s) in the search',
				'type'               => 'string',
				'default'            => '',
				'sanitize_callback'  => 'sanitize_text_field',
			),
			'team_location' => array(
				'description'        => 'The location(s) for the team(s) in the search',
				'type'               => 'string',
				'default'            => '',
				'sanitize_callback'  => 'sanitize_text_field',
			),
			'team_nickname' => array(
				'description'        => 'The nickname(s) for the team(s) in the search',
				'type'               => 'string',
				'default'            => '',
				'sanitize_callback'  => 'sanitize_text_field',
			),
			'team_abbreviation' => array(
				'description'        => 'The abbreviation(s) for the team(s) in the search',
				'type'               => 'string',
				'default'            => '',
				'sanitize_callback'  => 'sanitize_text_field',
			),
			'team_city' => array(
				'description'        => 'The team_city for the team(s) in the search',
				'type'               => 'string',
				'default'            => '',
				'sanitize_callback'  => 'sanitize_text_field',
			),
			'team_state' => array(
				'description'        => 'The state(s) for the team(s) in the search',
				'type'               => 'string',
				'default'            => '',
				'sanitize_callback'  => 'sanitize_text_field',
			),
			'team_head_coach' => array(
				'description'        => 'The head coach(es) for the team(s) in the search',
				'type'               => 'string',
				'default'            => '',
				'sanitize_callback'  => 'sanitize_text_field',
			),
			'team_division' => array(
				'description'        => 'The id(s) division(s) for the team(s) in the search',
				'type'               => 'integer',
				'default'            => 1,
				'sanitize_callback'  => 'absint',
			),
			'team_slug' => array(
				'description'        => 'The slug(s) for the team(s) in the search',
				'type'               => 'string',
				'default'            => '',
				'sanitize_callback'  => 'sanitize_text_field',
			),
			'team_active' => array(
				'description'        => 'Whether or not the team(s) are active',
				'type'               => 'string',
				'default'            => '',
				'sanitize_callback'  => 'sanitize_text_field',
			),
		);
	}

	public function get_item_schema() {
		$schema = array(
			'$schema'    => 'http://json-schema.org/draft-04/schema#',
			'title'      => 'entry',
			'type'       => 'object',
			'properties' => array(
				'team_id' => array(
					'description' => __( 'The id for the team.', 'sports-bench' ),
					'type'        => 'integer',
					'readonly'    => true,
				),
			),
		);
		return $schema;
	}

	protected function resgister_meta($data){
		$post_id = $data['id'];
		$r = array();

		if (function_exists('get_project_like_fnc')) {
			$r['likes'] = get_project_like_fnc($data['id']);
		}

		if (function_exists('get_project_views_fnc')) {
			$r['views'] = get_project_views_fnc($data['id']);
		}

		return $r;

	}

} ?>
