<?php

class Tada_Posts_Query extends WP_REST_Controller{

  public function get_items_permissions_check( $request ) {
    return apply_filters( 'wp_query_route_to_rest_api_permissions_check', true, $request );
  }

  public function query_post( $parameters ) {

    $default_args = array(
      'post_status'     => 'publish',
      'post_type'     => 'tada_project',
      'posts_per_page'  => 10,
      'has_password'    => false
    );
    $default_args = apply_filters( 'wp_query_route_to_rest_api_default_args', $default_args );

    // allow these args => what isn't explicitly allowed, is forbidden
    $allowed_args = array(
      'p',
      'name',
      'title',
      'page_id',
      'pagename',
      'post_parent',
      'post_parent__in',
      'post_parent__not_in',
      'post__in',
      'post__not_in',
      'post_name__in',
      'post_type', // With restrictions
      'posts_per_page', // With restrictions
      'offset',
      'paged',
      'page',
      'ignore_sticky_posts',
      'order',
      'orderby',
      'year',
      'monthnum',
      'w',
      'day',
      'hour',
      'minute',
      'second',
      'm',
      'date_query',
      'inclusive',
      'compare',
      'column',
      'relation',
      'post_mime_type',
      'lang', // Polylang
    );


    // Allow filtering by author: default yes
    if( apply_filters( 'wp_query_toute_to_rest_api_allow_authors', true ) ) {
      $allowed_args[] = 'author';
      $allowed_args[] = 'author_name';
      $allowed_args[] = 'author__in';
      $allowed_args[] = 'author__not_in';
    }

    // Allow filtering by meta: default yes
    if( apply_filters( 'wp_query_toute_to_rest_api_allow_meta', true ) ) {
      $allowed_args[] = 'meta_key';
      $allowed_args[] = 'meta_value';
      $allowed_args[] = 'meta_value_num';
      $allowed_args[] = 'meta_compare';
      $allowed_args[] = 'meta_query';
    }

    // Allow search: default yes
    if( apply_filters( 'wp_query_toute_to_rest_api_allow_search', true ) ) {
      $allowed_args[] = 's';
    }

    // Allow filtering by taxonomies: default yes
    if( apply_filters( 'wp_query_toute_to_rest_api_allow_taxonomies', true ) ) {
      $allowed_args[] = 'cat';
      $allowed_args[] = 'category_name';
      $allowed_args[] = 'category__and';
      $allowed_args[] = 'category__in';
      $allowed_args[] = 'category__not_in';
      $allowed_args[] = 'tag';
      $allowed_args[] = 'tag_id';
      $allowed_args[] = 'tag__and';
      $allowed_args[] = 'tag__in';
      $allowed_args[] = 'tag__not_in';
      $allowed_args[] = 'tag_slug__and';
      $allowed_args[] = 'tag_slug__in';
      $allowed_args[] = 'tax_query';
    }

    // let themes and plugins ultimately decide what to allow
    $allowed_args = apply_filters( 'wp_query_route_to_rest_api_allowed_args', $allowed_args );

    // args from url
    $query_args = array();

    foreach ( $parameters as $key => $value ) {

      // skip keys that are not explicitly allowed
      if( in_array( $key, $allowed_args ) ) {

        switch ( $key ) {

          // Posts type restrictions
          case 'post_type':

          // Multiple values
          if( is_array( $value ) ) {
            foreach ( $value as $sub_key => $sub_value ) {
              // Bail if there's even one post type that's not allowed
              if( !$this->check_is_post_type_allowed( $sub_value ) ) {
                $query_args[ $key ] = 'post';
                break;
              }
            }

            // Value "any"
          } elseif ( $value == 'any' ) {
            $query_args[ $key ] = $this->_get_allowed_post_types();
            break;

            // Single value
          } elseif ( !$this->check_is_post_type_allowed( $value ) ) {
            $query_args[ $key ] = 'post';
            break;
          }

          $query_args[ $key ] = $value;
          break;

          // Posts per page restrictions
          case 'posts_per_page':

          $max_pages = apply_filters( 'wp_query_route_to_rest_api_max_posts_per_page', 50 );
          if( $value <= 0 || $value > $max_pages ) {
            $query_args[ $key ] = $max_pages;
            break;
          }
          $query_args[ $key ] = $value;
          break;

          // Posts per page restrictions
          case 'posts_status':

          // Multiple values
          if( is_array( $value ) ) {
            foreach ( $value as $sub_key => $sub_value ) {
              // Bail if there's even one post status that's not allowed
              if( !$this->check_is_post_status_allowed( $sub_value ) ) {
                $query_args[ $key ] = 'publish';
                break;
              }
            }

            // Value "any"
          } elseif ( $value == 'any' ) {
            $query_args[ $key ] = $this->_get_allowed_post_status();
            break;

            // Single value
          } elseif ( !$this->check_is_post_status_allowed( $value ) ) {
            $query_args[ $key ] = 'publish';
            break;
          }

          $query_args[ $key ] = $value;
          break;

          // Set given value
          default:
          $query_args[ $key ] = $value;
          break;
        }
      }
    }

    // Combine defaults and query_args
    $args = wp_parse_args( $query_args, $default_args );

    // Make all the values filterable
    foreach ($args as $key => $value) {
      $args[$key] = apply_filters( 'wp_query_route_to_rest_api_arg_value', $value, $key, $args );
    }

    // Before query: hook your plugins here
    do_action( 'wp_query_route_to_rest_api_before_query', $args );

    // Run query
    $wp_query = new WP_Query( $args );
    // return $wp_query;
    // After query: hook your plugins here
    do_action( 'wp_query_route_to_rest_api_after_query', $wp_query );

    $data = array();

    while ( $wp_query->have_posts() ) : $wp_query->the_post();

    // Extra safety check for unallowed posts
    if ( $this->check_is_post_allowed( $wp_query->post ) ) {

      // Update properties post_type and meta to match current post_type
      // This is kind of hacky, but the parent WP_REST_Posts_Controller
      // does all kinds of assumptions from properties $post_type and
      // $meta so we need to update it several times.
      $this->post_type = $wp_query->post->post_type;
      $this->meta = new WP_REST_Post_Meta_Fields( $wp_query->post->post_type );
      $controller = new WP_REST_Posts_Controller($wp_query->post->post_type);

      // Use parent class functions to prepare the post
      $itemdata = $controller->prepare_item_for_response( $wp_query->post, $request );
      $data[]   = $controller->prepare_response_for_collection( $itemdata );
      // $itemdata = $controller->prepare_item_for_response( $wp_query->post, $request );
      // $data[]   = $controller->prepare_response_for_collection( $itemdata );
    }

  endwhile;

  return $this->get_response( $request, $args, $wp_query, $data );
}


protected function get_response( $request, $args, $wp_query, $data ) {

  $response = new WP_REST_Response( $data, 200 );
  $response->header( 'X-WP-Total', intval( $wp_query->found_posts ) );
  $max_pages = ( absint( $args[ 'posts_per_page' ] ) == 0 ) ? 1 : ceil( $wp_query->found_posts / $args[ 'posts_per_page' ] );
  $response->header( 'X-WP-TotalPages', intval( $max_pages ) );

  return $response;
}

protected function _get_allowed_post_status() {
  $post_status = array( 'any', 'publish', 'pending' );
  return apply_filters( 'wp_query_route_to_rest_api_allowed_post_status', $post_status );
}

protected function check_is_post_status_allowed( $post_status ) {
  return in_array( $post_status, $this->_get_allowed_post_status() );
}

protected function _get_allowed_post_types() {
  $post_types = get_post_types( array( 'show_in_rest' => true ) );
  return apply_filters( 'wp_query_route_to_rest_api_allowed_post_types', $post_types );
}

protected function check_is_post_type_allowed( $post_type ) {
  return in_array( $post_type, $this->_get_allowed_post_types() );
}

protected function check_is_post_allowed( $post ) {
  if( !$this->check_is_post_status_allowed( $post->post_status ) ) {
    return false;
  }
  if( !$this->check_is_post_type_allowed( $post->post_type ) ) {
    return false;
  }
  return apply_filters( 'wp_query_route_to_rest_api_post_is_allowed', true, $post );
}

public function insert_post($params, $files){

  $my_post = array(
    'post_title'    => $params['title'] ,
    'post_content'  => $params['content'],
    'post_status'   => 'publish',
    'post_type'   => 'tada_project',
    'post_author'   => get_current_user_id(),
  );

  // Insert the post into the database
  $post_id = wp_insert_post( $my_post );
  if ($post_id) {
    if (isset($params['tags'])) {
      wp_set_object_terms($post_id, $params['tags'], 'tada_item_tag');
    }
    if (isset($params['cat'])) {
      wp_set_object_terms($post_id, $params['cat'], 'tada_cat');
    }
    if (isset($files['thumbnail'])) {
      $this->insert_thumbnail_image($files, $post_id);
    }
    if (isset($files['images'])) {
      $this->insert_acf('images', $files , $post_id);
    }
    if (isset($files['files'])) {
      $this->insert_acf('files', $files , $post_id);
    }
  }
  return array($post_id);
}

public function update_post($params, $files){
  $my_post = array();
  $my_post['ID'] = $params['ID'];

  if ($params['title']) {
    $my_post['post_title'] = $params['title'];
  }

  if ($params['content']) {
    $my_post['post_content'] = $params['content'];
  }

  $post_id = wp_update_post( $my_post );

  if ($post_id) {
    if (isset($params['tags'])) {
      wp_set_object_terms($post_id, $params['tags'], 'tada_item_tag');
    }
    if (isset($params['cat'])) {
      wp_set_object_terms($post_id, $params['cat'], 'tada_cat');
    }
    if (isset($files['thumbnail'])) {
      $this->insert_thumbnail_image($files, $post_id);
    }
    if (isset($files['images'])) {
      $this->update_acf('images', $files , $post_id);
    }
    if (isset($files['files'])) {
      $this->update_acf('files', $files , $post_id);
    }
  }
  return array($post_id);
}

public function delete_post($post_id){
  $post = get_post( (int)$post_id );
  if ($post->post_author == get_current_user_id()) {
    return wp_delete_post( $post->ID, false);
  }
  return false;
}

protected function insert_thumbnail_image($files, $post_id){
  if ( ! function_exists( 'wp_handle_upload' ) ) {
    require_once( ABSPATH . 'wp-admin/includes/file.php' );
  }

  $upload_overrides = array( 'test_form' => false );
  $files = $files['thumbnail'];
  $uploadedfile = array(
    'name'     => $files['name'],
    'type'     => $files['type'],
    'tmp_name' => $files['tmp_name'],
    'error'    => $files['error'],
    'size'     => $files['size']
  );
  $uploadedfiles[] = $uploadedfile;
  $movefile = wp_handle_upload( $uploadedfile, $upload_overrides );
  if ( $movefile ) {
    $wp_filetype = $movefile['type'];
    $filename = $movefile['file'];
    $wp_upload_dir = wp_upload_dir();
    $attachment = array(
      'guid' => $wp_upload_dir['url'] . '/' . basename( $filename ),
      'post_mime_type' => $wp_filetype,
      'post_title' => preg_replace('/\.[^.]+$/', '', basename($filename)),
      'post_content' => '',
      'post_status' => 'inherit'
    );
    $attach_id = wp_insert_attachment( $attachment, $filename, $post_id);
  }
  if ($attach_id) {
    update_post_meta($post_id, '_thumbnail_id', $attach_id);
  }
}

protected function insert_acf ( $field, $files ,$post_id ) {

  if ( ! function_exists( 'wp_handle_upload' ) ) {
    require_once( ABSPATH . 'wp-admin/includes/file.php' );
  }
  $upload_overrides = array( 'test_form' => false );
  $files = $files[$field];

  foreach ((array)$files['name'] as $key => $value) {
    $uploadedfile = array(
      'name'     => $files['name'][$key],
      'type'     => $files['type'][$key],
      'tmp_name' => $files['tmp_name'][$key],
      'error'    => $files['error'][$key],
      'size'     => $files['size'][$key]
    );
    $uploadedfiles[] = $uploadedfile;
    $movefile = wp_handle_upload( $uploadedfile, $upload_overrides );
    if ( $movefile ) {
      $wp_filetype = $movefile['type'];
      $filename = $movefile['file'];
      $wp_upload_dir = wp_upload_dir();
      $attachment = array(
        'guid' => $wp_upload_dir['url'] . '/' . basename( $filename ),
        'post_mime_type' => $wp_filetype,
        'post_title' => preg_replace('/\.[^.]+$/', '', basename($filename)),
        'post_content' => '',
        'post_status' => 'inherit'
      );
      $attach_id = wp_insert_attachment( $attachment, $filename, $post_id);
    }

    if ($attach_id) {
      $attach_ids[] = $this->sub_filed($field, $attach_id);
    }
  }
  update_field($field , $attach_ids, $post_id);
}

protected function update_acf ( $field, $files ,$post_id ) {

  if ( ! function_exists( 'wp_handle_upload' ) ) {
    require_once( ABSPATH . 'wp-admin/includes/file.php' );
  }
  $upload_overrides = array( 'test_form' => false );
  $files = $files[$field];

  foreach ((array)$files['name'] as $key => $value) {
    $uploadedfile = array(
      'name'     => $files['name'][$key],
      'type'     => $files['type'][$key],
      'tmp_name' => $files['tmp_name'][$key],
      'error'    => $files['error'][$key],
      'size'     => $files['size'][$key]
    );

    $movefile = wp_handle_upload( $uploadedfile, $upload_overrides );
    if ( $movefile ) {
      $wp_filetype = $movefile['type'];
      $filename = $movefile['file'];
      $wp_upload_dir = wp_upload_dir();
      $attachment = array(
        'guid' => $wp_upload_dir['url'] . '/' . basename( $filename ),
        'post_mime_type' => $wp_filetype,
        'post_title' => preg_replace('/\.[^.]+$/', '', basename($filename)),
        'post_content' => '',
        'post_status' => 'inherit'
      );
      $attach_id = wp_insert_attachment( $attachment, $filename, $post_id);
    }

    if ($attach_id) {
      update_row($field, $key, $this->sub_filed($field, $attach_id),$post_id);
    }
  }
}

public function get_terms() {
  $taxonomies = get_terms( array(
    'taxonomy' => 'tada_cat',
    'hide_empty' => false
  ) );
  return $taxonomies;
}

protected function sub_filed($field, $attach_id){

  if ($field == 'files') {
    return array( 'file' => $attach_id );
  }
  return $attach_id;
}

public function plugin_compatibility_args( $args ) {

  $args[] = 'lang';

  return $args;
}


public function plugin_compatibility_after_query( $wp_query ) {

  if( function_exists( 'relevanssi_do_query' ) && !empty( $wp_query->query_vars[ 's' ] ) ) {
    relevanssi_do_query( $wp_query );
  }

}
}
