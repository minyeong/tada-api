<?php

/**
 * The plugin bootstrap file
 *
 * This file is read by WordPress to generate the plugin information in the plugin
 * admin area. This file also includes all of the dependencies used by the plugin,
 * registers the activation and deactivation functions, and defines a function
 * that starts the plugin.
 *
 * @link              https://wpteam.dev
 * @since             1.0.0
 * @package           Tada_Api
 *
 * @wordpress-plugin
 * Plugin Name:       tada-rest-api
 * Plugin URI:        tadastroy.com
 * Description:       타다 api 플러긴.
 * Version:           1.0.0
 * Author:            wpteam
 * Author URI:        https://wpteam.dev
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:       tada-api
 * Domain Path:       /languages
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

/**
 * Currently plugin version.
 * Start at version 1.0.0 and use SemVer - https://semver.org
 * Rename this for your plugin and update it as you release new versions.
 */
define( 'TADA_API_VERSION', '1.0.0' );

/**
 * The code that runs during plugin activation.
 * This action is documented in includes/class-tada-api-activator.php
 */
function activate_tada_api() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-tada-api-activator.php';
	Tada_Api_Activator::activate();
}

/**
 * The code that runs during plugin deactivation.
 * This action is documented in includes/class-tada-api-deactivator.php
 */
function deactivate_tada_api() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-tada-api-deactivator.php';
	Tada_Api_Deactivator::deactivate();
}
register_activation_hook( __FILE__, 'activate_tada_api' );
register_deactivation_hook( __FILE__, 'deactivate_tada_api' );

/**
 * The core plugin class that is used to define internationalization,
 * admin-specific hooks, and public-facing site hooks.
 */
require plugin_dir_path( __FILE__ ) . 'functions.php';
require plugin_dir_path( __FILE__ ) . 'includes/class-tada-api.php';


/**
 * Begins execution of the plugin.
 *
 * Since everything within the plugin is registered via hooks,
 * then kicking off the plugin from this point in the file does
 * not affect the page life cycle.
 *
 * @since    1.0.0
 */
function run_tada_api() {

	$plugin = new Tada_Api();
	$plugin->run();

}
run_tada_api();
